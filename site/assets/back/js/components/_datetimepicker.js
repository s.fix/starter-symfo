require('jquery-ui/ui/widgets/datepicker');
require('jquery-ui/ui/i18n/datepicker-fr');

const NAME = 'datepicker';
const DATA_KEY = 'bs.datepicker';
const DATA_API_KEY = '.data-api';
const EVENT_KEY = `.${DATA_KEY}`;

const Event = {
    LOAD_DATA_API: `load${EVENT_KEY}${DATA_API_KEY}`
};

const Selector = {
    DATA_DATE: '[data-datepicker="date"]',
    DATA_DATE_BIRTHDAY: '[data-datepicker="birthday"]'
};

$.datepicker.setDefaults($.datepicker.regional["fr"]);

$.datepicker.setDefaults({
    showWeek: true,
    changeYear: true
});

$(window).on(Event.LOAD_DATA_API, () => {
    const dates = [].slice.call(document.querySelectorAll(Selector.DATA_DATE))
    for (let i = 0, len = dates.length; i < len; i++) {
        const date = $(dates[i])
        date.datepicker({
            yearRange: 'c-100:c+100',
        });
    }
    const birthdayDates = [].slice.call(document.querySelectorAll(Selector.DATA_DATE_BIRTHDAY))
    for (let i = 0, len = birthdayDates.length; i < len; i++) {
        const date = $(birthdayDates[i])
        date.datepicker({
            yearRange: 'c-150:c+n',
            maxDate: '0d'
        });
    }
});
