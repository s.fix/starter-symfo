require('select2');

$(document).ready(function() {
    $('select').each(function () {
        $(this).select2({
            theme: 'bootstrap4',
            width: '100%',
            placeholder: $(this).attr('placeholder'),
            allowClear: Boolean($(this).data('allow-clear')),
        });
    });
});