const NAME = 'add-field-email';
const DATA_KEY = 'bs.add-field-email';
const DATA_API_KEY = '.data-api';
const EVENT_KEY = `.${DATA_KEY}`;

const Event = {
    CLICK_DATA_API: "click" + EVENT_KEY + DATA_API_KEY
};

const Selector = {
    DATA_EMAIL: '[data-add-field="email"]',
    DATA_BTN_REMOVE: '[btn-delete="email"]'
};

//the related data is removed from the collection
//https://symfony.com/doc/current/form/form_collections.html#allowing-tags-to-be-removed
function addTagFormDeleteLink($tagFormLi) {
    const $removeFormButton = $(Selector.DATA_BTN_REMOVE);

    $removeFormButton.on('click', function (e) {
        $tagFormLi.remove();
    });
}

$(document).on(Event.CLICK_DATA_API, Selector.DATA_EMAIL, function (event) {
    event.preventDefault();
    const email = event.target;

    const list = $($(email).attr('data-list-selector'));
    // Try to find the counter of the list or use the length of the list
    let counter = list.data('widget-counter') | list.children().length;

    // grab the prototype template
    let newWidget = list.attr('data-prototype');
    newWidget = newWidget.replace(/__name__/g, counter);
    counter++;
    list.data('widget-counter', counter);

    const newElem = $(list.attr('data-widget-tags').replace(/__name__/g, counter)).html(newWidget);
    newElem.appendTo(list);
    addTagFormDeleteLink(newElem);
});
