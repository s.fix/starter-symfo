const NAME = 'file';
const DATA_KEY = 'bs.file';
const DATA_API_KEY = '.data-api';
const EVENT_KEY = `.${DATA_KEY}`;

const Event = {
    LOAD_DATA_API: `load${EVENT_KEY}${DATA_API_KEY}`
};

const Selector = {
    DATA_FILE: '[data-file="file"]'
};

$(window).on(Event.LOAD_DATA_API, () => {
    const files = [].slice.call(document.querySelectorAll(Selector.DATA_FILE))
    for (let i = 0, len = files.length; i < len; i++) {
        const $file = $(files[i])
        $file.change(function (e) {
            const filesLength = e.target.files.length;

            const nomFichier = (filesLength > 1) ? filesLength + " fichiers" :
                (filesLength === 1) ? e.target.files[0].name : 'Sélectionner un fichier';

            $(this).next('.custom-file-label').text(nomFichier);
        });
    }
});