// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.scss');

const $ = require('jquery');
// this "modifies" the jquery module: adding behavior to it
// the bootstrap module doesn't export/return anything
require('bootstrap');

// or you can include specific pieces
// require('bootstrap/js/dist/tooltip');
// require('bootstrap/js/dist/popover');

$(document).ready(function () {
    $('[data-toggle="popover"]').popover();

    $("input[type=file]").change(function (e) {
        const filesLength = e.target.files.length;

        const nomFichier = (filesLength > 1) ? filesLength + " fichiers" :
            (filesLength === 1) ? e.target.files[0].name : 'Sélectionner un fichier';

        $(this).next('.custom-file-label').text(nomFichier);
    })
});