<?php

namespace App\Domain\CatalogueContext;

use Doctrine\Common\Collections\ArrayCollection;

class Produit
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $libelle;

    /**
     * @var string
     */
    private $description;

    /**
     * @var ArrayCollection|Categorie[]
     */
    private $categories;

}
