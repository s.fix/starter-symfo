<?php

namespace App\Domain\CatalogueContext;

use Doctrine\Common\Collections\ArrayCollection;

class Categorie
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $libelle;

    /**
     * @var string
     */
    private $description;

    /**
     * @var Catalogue
     */
    private $catalogue;

    /**
     * @var ArrayCollection|Produit[]
     */
    private $produits;
}
