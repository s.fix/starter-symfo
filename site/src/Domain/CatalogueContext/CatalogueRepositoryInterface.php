<?php

namespace App\Domain\CatalogueContext;

interface CatalogueRepositoryInterface
{
    public function fetchCategoriesByCatalogue(Catalogue $catalogue);
}
