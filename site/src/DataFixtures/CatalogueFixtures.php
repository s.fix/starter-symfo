<?php

namespace App\DataFixtures;

use App\Domain\CatalogueContext\CatalogueRepositoryInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CatalogueFixtures extends Fixture
{
    /**
     * @var CatalogueRepositoryInterface
     */
    private $catalogueRepository;

    /**
     * CatalogueFixtures constructor.
     *
     * @param CatalogueRepositoryInterface $catalogueRepository
     */
    /*
    public function __construct(CatalogueRepositoryInterface $catalogueRepository)
    {

        $this->catalogueRepository = $catalogueRepository;
    }
    */

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /*
        $catalogue = CatalogueFactory::create('libelle', 'description');
        $this->catalogueRepository->save($catalogue);
        */
    }
}
