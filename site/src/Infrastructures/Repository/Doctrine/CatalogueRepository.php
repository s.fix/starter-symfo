<?php

namespace App\Infrastructures\Repository\Doctrine;

use App\Domain\Model\Catalogue\Catalogue;
use App\Domain\Repository\CatalogueRepositoryInterface;

class CatalogueRepository implements CatalogueRepositoryInterface
{
    public function fetchCategoriesByCatalogue(Catalogue $catalogue)
    {
        return [];
    }
}
