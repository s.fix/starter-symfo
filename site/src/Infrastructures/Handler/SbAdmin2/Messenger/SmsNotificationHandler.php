<?php

namespace App\Infrastructures\Handler\SbAdmin2\Messenger;

use App\Domain\Messenger\SmsNotification;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class SmsNotificationHandler implements MessageHandlerInterface
{
    public function __invoke(SmsNotification $message)
    {
        sleep(10);
        echo $message->getContent();
    }
}
