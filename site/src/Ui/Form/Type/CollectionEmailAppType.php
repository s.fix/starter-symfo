<?php

namespace App\Ui\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CollectionEmailAppType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'entry_type' => EmailAppType::class,
            'allow_add' => true,
            'allow_delete' => true,
        ]);
    }

    public function getParent()
    {
        return CollectionType::class;
    }
}
