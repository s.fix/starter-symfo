<?php

namespace App\Ui\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CheckboxMultipleType extends AbstractType
{
    public const ENGLISH = 0;
    public const SPANISH = 10;
    public const BORK = 20;
    public const PIRATE = 30;

    public function configureOptions(OptionsResolver $resolver)
    {
        $choices = [
            'English' => self::ENGLISH,
            'Spanish' => self::SPANISH,
            'Bork' => self::BORK,
            'Pirate' => self::PIRATE,
        ];
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'expanded' => true,
            'multiple' => true,
            'choices' => $choices,
        ]);
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}
