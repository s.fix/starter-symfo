<?php

namespace App\Ui\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateIntervalType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DateIntervalAppType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'with_years' => false,
            'with_months' => false,
            'with_days' => true,
            'with_hours' => true,
            'labels' => [
                'days' => 'Jours',
                'hours' => 'Heures',
            ],
        ]);
    }

    public function getParent()
    {
        return DateIntervalType::class;
    }
}
