<?php

namespace App\Ui\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FileAppType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'attr' => [
                'placeholder' => 'Sélectionner un fichier',
            ],
        ]);
    }

    public function getParent()
    {
        return FileType::class;
    }
}
