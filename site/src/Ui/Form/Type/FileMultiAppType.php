<?php

namespace App\Ui\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FileMultiAppType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'attr' => [
                'placeholder' => 'Sélectionner fichier(s)',
            ],
            'multiple' => true,
        ]);
    }

    public function getParent()
    {
        return FileType::class;
    }
}
