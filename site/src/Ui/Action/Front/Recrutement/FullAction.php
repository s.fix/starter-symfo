<?php

namespace App\Ui\Action\Front\Recrutement;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class FullAction
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        $content = $this->twig->render(('front/home/homepage.html.twig'));
        return new Response($content);
    }
}