<?php

namespace App\Ui\Action\Back\Catalogue;

use App\Application\UseCase\Catalogue\AjouterCategorie\AjouterCategorieUseCase;
use App\Ui\Action\ActionInterface;

class CatalogueCreateAction implements ActionInterface
{
    private $service;

    public function __construct(AjouterCategorieUseCase $service)
    {
        $this->service = $service;
    }

    /**
     *
     */
    public function __invoke()
    {

    }

}