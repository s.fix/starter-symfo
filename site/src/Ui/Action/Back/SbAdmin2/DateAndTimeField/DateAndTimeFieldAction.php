<?php

namespace App\Ui\Action\Back\SbAdmin2\DateAndTimeField;

use App\Ui\Form\Type\DateBirthdayAppType;
use App\Ui\Form\Type\DateIntervalAppType;
use App\Ui\Form\Type\DatePickerAppType;
use App\Ui\Form\Type\DateTimeAppType;
use App\Ui\Form\Type\TimeAppType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class DateAndTimeFieldAction extends AbstractController
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @Route("/demo/date_and_time_field/full", name="date_and_time_field_demo")
     *
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        $form = $this->createFormBuilder()
            ->add('DateType', DatePickerAppType::class)
            ->add('remindEvery', DateIntervalAppType::class)
            ->add('DateTimeType', DateTimeAppType::class)
            ->add('TimeType', TimeAppType::class)
            ->add('BirthdayType', DateBirthdayAppType::class)
            ->getForm();

        $content = $this->twig->render('back/date_and_time_field/full.html.twig',
            [
                'form' => $form->createView(),
            ]
        );

        return new Response($content);
    }
}
