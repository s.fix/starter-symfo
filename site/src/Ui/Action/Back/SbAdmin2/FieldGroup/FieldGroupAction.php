<?php

namespace App\Ui\Action\Back\SbAdmin2\FieldGroup;

use App\Ui\Form\Type\CollectionEmailAppType;
use App\Ui\Form\Type\RepeatedPasswordAppType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class FieldGroupAction extends AbstractController
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @Route("/demo/field_group/full", name="field_group_demo")
     *
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        $form = $this->createFormBuilder()
            ->add('emails', CollectionEmailAppType::class)
            ->add('password', RepeatedPasswordAppType::class)
            ->getForm();

        $content = $this->twig->render('back/field_group/full.html.twig',
            [
                'form' => $form->createView(),
            ]
        );

        return new Response($content);
    }
}
