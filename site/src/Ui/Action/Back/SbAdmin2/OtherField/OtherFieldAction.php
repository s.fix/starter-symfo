<?php

namespace App\Ui\Action\Back\SbAdmin2\OtherField;

use App\Ui\Form\Type\CheckboxMultipleType;
use App\Ui\Form\Type\CheckboxShowThisType;
use App\Ui\Form\Type\FileAppType;
use App\Ui\Form\Type\FileMultiAppType;
use App\Ui\Form\Type\RadioMultipleType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class OtherFieldAction extends AbstractController
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @Route("/demo/other_field/full", name="other_field_demo")
     *
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        $form = $this->createFormBuilder()
            ->add('public', CheckboxShowThisType::class)
            ->add('checkboxMultiple', CheckboxMultipleType::class)
            ->add('attachment', FileAppType::class)
            ->add('attachmentMultiple', FileMultiAppType::class)
            ->add('RadioType', RadioType::class)
            ->add('radioMultiple', RadioMultipleType::class)
            ->getForm();

        $content = $this->twig->render('back/other_field/full.html.twig',
            [
                'form' => $form->createView(),
            ]
        );

        return new Response($content);
    }
}
