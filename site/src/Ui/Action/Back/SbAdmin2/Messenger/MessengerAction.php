<?php

namespace App\Ui\Action\Back\SbAdmin2\Messenger;

use App\Domain\Messenger\SmsNotification;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class MessengerAction extends AbstractController
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @Route("/demo/messenger/full", name="messenger_demo")
     *
     * @return Response
     */
    public function __invoke(Request $request, MessageBusInterface $bus): Response
    {
        $bus->dispatch(new SmsNotification('test messenger'));

        $content = $this->twig->render(('back/messenger/full.html.twig'));

        return new Response($content);
    }
}
