<?php

namespace App\Ui\Action\Back\SbAdmin2\Bouton;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class BoutonAction
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @Route("/demo/bouton/full", name="bouton_demo")
     *
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        $content = $this->twig->render(('back/bouton/full.html.twig'));

        return new Response($content);
    }
}
