<?php

namespace App\Ui\Action\Back\SbAdmin2\MotDePasseOublie;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class MotDePasseOublieAction
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @Route("/demo/mot_de_passe_oublie/full", name="mot_de_passe_oublie_demo")
     *
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        $content = $this->twig->render(('back/mot_de_passe_oublie/full.html.twig'));

        return new Response($content);
    }
}
