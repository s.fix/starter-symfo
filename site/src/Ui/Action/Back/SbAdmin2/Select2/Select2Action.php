<?php

namespace App\Ui\Action\Back\SbAdmin2\Select2;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class Select2Action
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @Route("/demo/select_2/full", name="select_2_demo")
     *
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        $content = $this->twig->render(('back/select_2/full.html.twig'));

        return new Response($content);
    }
}
