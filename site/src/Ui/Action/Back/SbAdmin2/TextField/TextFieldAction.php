<?php

namespace App\Ui\Action\Back\SbAdmin2\TextField;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class TextFieldAction extends AbstractController
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @Route("/demo/text_field/full", name="text_field_demo")
     *
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        $form = $this->createFormBuilder()
            ->add('TextType', TextType::class)
            ->add('TextareaType', TextareaType::class)
            ->add('EmailType', EmailType::class)
            ->add('IntegerType', IntegerType::class)
            ->add('MoneyType', MoneyType::class)
            ->add('NumberType', NumberType::class)
            ->add('PasswordType', PasswordType::class)
            ->add('PercentType', PercentType::class)
            ->add('SearchType', SearchType::class)
            ->add('UrlType', UrlType::class)
            ->add('RangeType', RangeType::class)
            ->add('TelType', TelType::class)
            ->add('ColorType', ColorType::class)
            ->getForm()
        ;

        $content = $this->twig->render('back/text_field/full.html.twig',
            [
                'form' => $form->createView(),
            ]
        );

        return new Response($content);
    }
}
