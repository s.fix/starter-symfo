<?php

namespace App\Ui\Action\Back\SbAdmin2\ChoiceField;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\CurrencyType;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;
use Symfony\Component\Form\Extension\Core\Type\LocaleType;
use Symfony\Component\Form\Extension\Core\Type\TimezoneType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class ChoiceFieldAction extends AbstractController
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @Route("/demo/choice_field/full", name="choice_field_demo")
     *
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        $choices = [
            'English' => 'en',
            'Spanish' => 'es',
            'Bork' => 'muppets',
            'Pirate' => 'arr',
        ];

        $form = $this->createFormBuilder()
            ->add('ChoiceType', ChoiceType::class, ['choices' => $choices])
            ->add('ChoiceTypeMultiple', ChoiceType::class, [
                'choices' => $choices,
                'multiple' => true,
            ])
            ->add('CountryType', CountryType::class)
            ->add('LanguageType', LanguageType::class)
            ->add('LocaleType', LocaleType::class)
            ->add('TimezoneType', TimezoneType::class)
            ->add('CurrencyType', CurrencyType::class)

            ->getForm()
        ;

        $content = $this->twig->render('back/choice_field/full.html.twig',
            [
                'form' => $form->createView(),
            ]
        );

        return new Response($content);
    }
}
