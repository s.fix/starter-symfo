<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use PHPUnit\Framework\Assert;
use App\Domain\Catalogue\Catalogue;
use App\Infrastructures\Repositories\Catalogue\CatalogueRepositoryInMemory;

/**
 * Defines application features from the specific context.
 */
class DomainContext implements Context
{
    private $catalogue;

    /** @var CatalogueRepositoryInterface */
    private $repository;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        $this->repository = new CatalogueRepositoryInMemory();
    }

        /**
     * @Given un administrateur
     */
    public function unAdministrateur()
    {
        
    }

    /**
     * @When je créer un nouveau catalogue
     */
    public function jeCreerUnNouveauCatalogue()
    {
        $this->catalogue = new Catalogue();
    }

    /**
     * @Then il contient aucune catégorie
     */
    public function ilContientAucuneCategorie()
    {
        $categories = $this->repository->fetchCategorieByCatalogue($this->catalogue);
        Assert::assertEquals(0, count($categories));
    }

}
